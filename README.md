## SyncUp

![Current Version](https://img.shields.io/badge/version-v0.1-blue)
![GitHub contributors](https://img.shields.io/github/contributors/madhur-taneja/README-Template)
![GitHub stars](https://img.shields.io/github/stars/madhur-taneja/README-Template?style=social)
![GitHub forks](https://img.shields.io/github/forks/madhur-taneja/README-Template?style=social)

- Paul Wu
- Donovan McCroddan
- Michael Ford
- Gavin Griffith

SyncUp is a user-friendly scheduling app for creating meetings and group events. It simplifies coordination, allowing users to effortlessly schedule meetings with one another across timezones. Say goodbye to scheduling hassles with SyncUp.

## Deployment

This application is 100% fully deployed and functional

## Design

- [API Design](docs/apiEndpoint.MD)
- [Models](docs/models.MD)
- [Integrations](docs/integrations.MD)

## Intended Market

Anyone from students to professionals, younger or older. Our app is intended for general use that can be geared towards casual everyday life or a fast paced work schedule where you need a little extra help keeping up with your appointments.

## Project Initialization

To run this application on your local machine, please follow these steps:

1. Run docker volume create postgres-data
2. Run docker compose build
3. Run docker compose up
4. Run docker exec -it fastapi-1 bash
5. Run python -m migrations up
6. Exit the container's CLI
7. Run docker exec -it vite_docker bash
8. Run npm install

## File Tree's

- [File Tree](docs/fileTree.MD)

## Functionality

- Our Main page provides a quick rundown of our app along with some fun links ;)
- Users can sign up from our main page
  - Users must provide a Email(as their username), Password, Name, and their Timezone which can be selected from a drop down list
  - Users can also choose to provide a picture URL (Must be a valid URL)
- Users can login from our Main page using a Modal
- Users can logout from any page
- Our home page will display a list of meetings happening that day for the user or tell you to get a job
  - The home page will also allow you to update your profile
- The group home will allow you to see all of the groups that you're in
  - The group must be selected using a modal to view the group's details, including news events
    - Group details will display a list of users in the group, as well as their admin status
    - User can not add the same user into the group twice.
    - While viewing the group you can add and delete users to and from the group
  - From the group home page you can create meetings
    - The meeting creation page asks for a name, description, date and time, and gives an option for a link or location for the meeting
- The meeting home page will display a list of all meetings the user is in
  - Users can click on the meeting to be taken to the meeting detail page, where you can update the meeting, and add users, and view meeting details
  - User can not add the same user, or user not in the group into the meeting that belong to certain group
  - Users can always leave a meeting, but only admins can delete meetings

## Journal

-[Journal](journals/journal.MD) Please Read!

## Wish List

- use AWS EC2 bucket to store our picture.

- love to implement the availability for user, and make them have the choice of different time to have the meeting. As well as block out all the time they already have the meeting.

- Wish to add a automatic email service that send email to our user when they get a new meeting.

- To add live chat for user who is in one group

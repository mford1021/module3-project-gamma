fastapi[all]==0.92.0
uvicorn[standard]==0.22.0
pytest
psycopg[binary,pool]==3.1.9
jwtdown-fastapi==0.5.0

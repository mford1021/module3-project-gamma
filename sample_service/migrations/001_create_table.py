steps = [
    [
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(100) NOT NULL,
            email VARCHAR(100) NOT NULL UNIQUE,
            hashed_password VARCHAR(100) NOT NULL,
            timezone VARCHAR(5) NOT NULL,
            picture TEXT
        )
        """,
        """
        DROP TABLE users;
        """,
    ],
    [
        """
        CREATE TABLE groups (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(100) NOT NULL,
            news_event TEXT
        )
        """,
        """
        DROP TABLE groups;
        """,
    ],
    [
        """
        CREATE TABLE meeting (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(1000) NOT NULL,
            descriptions TEXT,
            link TEXT,
            time TEXT NOT NULL,
            group_id INTEGER REFERENCES groups("id") ON DELETE CASCADE
        )
        """,
        """
        DROP TABLE meeting;
        """,
    ],
    [
        """
        CREATE TABLE u_m_linker (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INTEGER REFERENCES users("id") ON DELETE CASCADE,
            meeting_id INTEGER REFERENCES meeting("id") ON DELETE CASCADE,
            agreed_to BOOLEAN NOT NULL DEFAULT TRUE
        )
        """,
        """
        DROP TABLE u_m_linker;
        """,
    ],
    [
        """
        CREATE TABLE g_u_linker (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INTEGER REFERENCES users("id") ON DELETE CASCADE,
            group_id INTEGER REFERENCES groups("id") ON DELETE CASCADE,
            admin_status BOOLEAN NOT NULL
        )
        """,
        """
        DROP TABLE g_u_linker;
        """,
    ],
]

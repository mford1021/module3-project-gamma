PLEASE USE REDUX GUYS

# main page

we might not need this
endpoint: @app.get("/api/main")
Headers:
if user log in, redirect to home page check cookie
Request Shape:
nothing, maybe animation library ?
Response:
a main page of our product, what to do, about us, reviews, login, sign up, links to our social

# log in

endpoint:@app.post(/api/login)
Headers:

- Authorization:
  -token for log in that last for a period
- Request shape (form):
  - username: string
  - password: string
- Response Shape:

```json
{
  "email": "string",
  "password": "string",
  "token": "string"
}
```

have this like this for easier to look if they are same thing

# reset password

- endpoint:@app.put(/api/user)
- json for forget my password
- json new password and username

# sign up

endpoint:@app.post(/api/user)
Headers:author

- Request shape (form):
  - username: string
  - email:string,
  - password: string
  - security question: object
  - timezone: Datetime

remember to set the availability to all free if not add any

```json
{
  "email": "string",
  "name": "string",
  "password": "string",
  "security question": {
    "question": "answer"
  },
  "timezone": "DateTime"
}
```

# user/home

REMEMBER NO NEED TO GET THE GROUP FOR NOW!!!!!!

endpoint: @app.get("/api/{user_id}")

- Query parameters:

  - use_id: the user

- Headers:

  - token for login

- Response
  ```json
  {
    "meeting_name": "string",
    "meeting_id": "id",
    "meeting_time": "datetime",
    "user_availability": "datetime"
  }
  ```

# user/home/meeting_id

endpoint: @app.get("/api/meeting/{meeting_id}")

- Query parameters:

  - meeting_id: the meeting user looking at

- Response

```json
{
  "meeting_description": "string",
  "meeting_id": "id",
  "meeting_time": "datetime",
  "meeting_person": "list of user_name, user_id"
}
```

?admin status

# meeting/create

endpoint: @app.post("/api/meeting/")
Headers: is admin

- Request shape (form):

  - meeting_time: string,
  - meeting_description:string,
  - meeting_person: list of objects of user
  - meeting or event: true or false

- response shape

```json
{
  "meeting_description": "string",
  "meeting_id": "id",
  "meeting_time": "datetime",
  "meeting_person": [
    {"user":"user_id,user_name,user_availability"},
    {"user":"user_id,user_name,user_availability"},
    {"user":"user_id,user_name,user_availability"}
    ],
  "meeting_or_event": true or false,
  "is_admin": true or false
}
```

endpoint: @app.get("/api/group_id/")

- check all event for this group
- and news
  response shape

Request shape (form):

- group_id: id,

- Response

  ```json
  {
    "user of a group": [
      { "user": "user_id,user_name" },
      { "user": "user_id,user_name" },
      { "user": "user_id,user_name" }
    ],
    "group_new": "string of news",
    "group_meeting": "only you can see your meeting you will see the user_id of people at the meeting",
    "group_name": "name of the group"
  }
  ```

  endpoint: @app.put("/api/group_id/")

  - add people to 1 group
  - add news/modify news

  ```json
  {
    "user": { "user_object": "user_object" },
    "news": "a news"
  }
  ```

- Response for the new meeting:
  - json with
    - time
    - group
    - description
    - meeting name
    - person in the meeting
    - is required
    - don't care about others
    - time zone !! not from user from us

# group/view

endpoint: @app.get("/api/{user_id}/groups")
response shape =

```json
{
  "groups": [
    { "group": "group_id+group_info" },
    { "group": "group_id+group_info" },
    { "group": "group_id+group_info" }
  ]
}
```

- endpoint: @app.post("/api/groups")
  request form: json()

  - Headers:
    - admin status
    - token for login
      response

- Query parameters:

  - group_id: the group we are checking out now
  - use_id: the user

  ```json
  {
    "users":[{"user_name,user_email"},{"user_name,user_email"},{"user_name,user_email"}],
    "group_name":"name of the group",
    "group_id":"we have it"
  }
  ```

# availability change setting

endpoint: @app.put("/api/{user_id}")

- use_id: the user

- Headers:
  - token for login
- Response
  -current availability
  -change availability
  -current user info
  -change user info
  -delete user

endpoint: @app.delete("/api/{user_id}")

- Query parameters:

  - use_id: the user

  ```json
  {
    "email": "string",
    "name": "string",
    "password": "string",
    "security question": {
      "question": "answer"
    },
    "timezone": "DateTime",
    "availability": "DateTime"
  }
  ```

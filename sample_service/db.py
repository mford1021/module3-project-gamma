import os
from psycopg_pool import ConnectionPool
from api.routers.models import GroupIn, GroupOut, UserOut, User

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class UserQueries:
    def get_all_users(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT *
                    FROM users
                """
                )
                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)
                return results

    def create_user(self, user: User, hashed_password: str) -> UserOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO users (name,
                    email, hashed_password, timezone, picture)
                    VALUES (%s, %s, %s, %s, %s)
                    RETURNING id, email, name,
                    hashed_password, timezone, picture
                    """,
                    [
                        user.name,
                        user.email,
                        hashed_password,
                        user.timezone,
                        user.picture,
                    ],
                )
                id = result.fetchone()[0]
                return UserOut(
                    id=id,
                    email=user.email,
                    hashed_password=hashed_password,
                    name=user.name,
                    timezone=user.timezone,
                    picture=user.picture,
                )

    def update_user(self, id, data, hashed_password):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.name,
                    data.picture,
                    data.email,
                    data.timezone,
                    hashed_password,
                    id,
                ]
                cur.execute(
                    """
                    UPDATE users
                    SET name = %s,
                    picture = %s,
                    email = %s,
                    timezone = %s,
                    hashed_password = %s
                    WHERE id = %s
                    RETURNING id, name, picture,
                    email, timezone, hashed_password
                    """,
                    params,
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                return record

    def delete_user(self, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM users
                    WHERE id = %s
                    """,
                    [user_id],
                )

    def get_user(self, email):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT *
                    FROM users
                    WHERE email = %s
                    """,
                    [email],
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]

                return UserOut(
                    id=record["id"],
                    email=record["email"],
                    hashed_password=record["hashed_password"],
                    name=record["name"],
                    timezone=record["timezone"],
                    picture=record["picture"],
                )


class GroupQueries:
    def create_group(self, user_id: int, group_data: GroupIn):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO groups (name)
                    VALUES (%s)
                    RETURNING id, name
                    """,
                    [group_data.name],
                )
                group_id = result.fetchone()[0]
                db.execute(
                    """
                    INSERT INTO g_u_linker (group_id, user_id, admin_status)
                    VALUES (%s, %s, True)
                    """,
                    [group_id, user_id],
                )
                return GroupOut(id=group_id, name=group_data.name)

    def get_all_groups(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id,name,news_event
                    FROM groups
                    """
                )
                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)
                return results

    def update_group(self, group_id: int, data):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.name,
                    data.news_event,
                    group_id,
                ]
                cur.execute(
                    """
                    UPDATE groups
                    SET name = %s,
                    news_event = %s
                    WHERE id = %s
                    RETURNING id, name, news_event
                    """,
                    params,
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
            return record

    def get_g_u_linker(self, group_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT *
                    FROM g_u_linker
                    WHERE group_id = %s
                    """,
                    [group_id],
                )
                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)
                return results

    def get_one_group(self, group_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT groups.id, groups.name,
                    groups.news_event, g_u_linker.user_id,
                    g_u_linker.admin_status
                    FROM groups
                    LEFT JOIN g_u_linker ON groups.id = g_u_linker.group_id
                    WHERE groups.id = %s
                    """,
                    [group_id],
                )
                record = None
                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)
                return results

    def add_user_to_group(self, user_id, group_id, data):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                SELECT user_id, group_id
                FROM g_u_linker
                WHERE user_id = %s AND group_id = %s
                """,
                    [user_id, group_id],
                )
                if cur.fetchone() is not None:
                    raise ValueError
                cur.execute(
                    """
                INSERT INTO g_u_linker (user_id,group_id,admin_status)
                VALUES (%s, %s, %s)
                RETURNING user_id, group_id, admin_status
                """,
                    [user_id, group_id, data],
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
            return record

    def delete_user_from_group(self, user_id, group_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                        DELETE FROM g_u_linker
                        WHERE user_id = %s AND group_id = %s
                        """,
                    [user_id, group_id],
                )

    def delete_group(self, group_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                            DELETE FROM g_u_linker
                            WHERE group_id = %s
                            """,
                    [group_id],
                )
                cur.execute(
                    """
                            DELETE FROM groups
                            WHERE id = %s
                            """,
                    [group_id],
                )

    def get_users_from_group(self, group_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT user_id
                    FROM g_u_linker
                    WHERE group_id = %s
                    """,
                    [group_id],
                )
                user_ids = [row[0] for row in cur.fetchall()]

            name_list = []
            with conn.cursor() as cur:
                for user_id in user_ids:
                    cur.execute(
                        """
                        SELECT name, id, email
                        FROM users
                        WHERE id = %s
                        """,
                        [user_id],
                    )
                    c = cur.fetchone()
                    user_info = {"name": c[0], "id": c[1], "email": c[2]}
                    name_list.append(user_info)

            return name_list

    def update_user_status(self, admin_status, user_id, group_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    UPDATE g_u_linker
                    SET admin_status = %s
                    WHERE user_id = %s AND group_id = %s
                    RETURNING user_id, group_id, admin_status
                    """,
                    [admin_status, user_id, group_id],
                )
                record = None
                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)
                return results

    def get_user_status(self, user_id, group_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT *
                    FROM g_u_linker
                    WHERE user_id = %s AND group_id = %s
                    """,
                    [user_id, group_id],
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
            return record

    def all_groups_from_user(self, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT group_id
                    FROM g_u_linker
                    WHERE user_id = %s
                    """,
                    [user_id],
                )
                group_ids = [row[0] for row in cur.fetchall()]

            group_list = []
            with conn.cursor() as cur:
                for group_id in group_ids:
                    cur.execute(
                        """
                        SELECT *
                        FROM groups
                        WHERE id = %s
                        """,
                        [group_id],
                    )
                    c = cur.fetchone()
                    group_info = {
                        "id": c[0],
                        "name": c[1],
                        "news_event": c[2],
                    }
                    group_list.append(group_info)

            return group_list


class MeetingQueries:
    def create_meeting(self, meeting_in, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    INSERT INTO meeting (name, descriptions,
                    link, time, group_id)
                    VALUES(%s,%s,%s,%s,%s)
                    RETURNING id, name,descriptions,link,time, group_id
                    """,
                    [
                        meeting_in.name,
                        meeting_in.descriptions,
                        meeting_in.link,
                        meeting_in.time,
                        meeting_in.group_id,
                    ],
                )
                meeting = cur.fetchone()
                description = cur.description
                meeting_id = meeting[0]
            with conn.cursor() as cur:
                cur.execute(
                    """
                    INSERT INTO u_m_linker (user_id,meeting_id)
                    VALUES(%s,%s)
                    """,
                    [user_id, meeting_id],
                )
            record = None
            row = meeting
            if row is not None:
                record = {}
                for i, column in enumerate(description):
                    record[column.name] = row[i]
        return record

    def get_meeting(self, meeting_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT *
                    FROM meeting
                    WHERE id = %s
                    """,
                    [meeting_id],
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
            return record

    def update_meeting(self, meeting_id, meeting_data):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    UPDATE meeting
                    SET name = %s,
                    descriptions = %s,
                    link = %s,
                    time = %s
                    WHERE id = %s
                    RETURNING name, descriptions, link, time, id
                    """,
                    [
                        meeting_data.name,
                        meeting_data.descriptions,
                        meeting_data.link,
                        meeting_data.time,
                        meeting_id,
                    ],
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
            return record

    def delete_meeting(self, meeting_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                        DELETE FROM meeting
                        WHERE id = %s
                        """,
                    [meeting_id],
                )

    def add_user_to_meeting(self, user_id, meeting_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                SELECT user_id, meeting_id
                FROM u_m_linker
                WHERE user_id = %s AND meeting_id = %s
                """,
                    [user_id, meeting_id],
                )
                if cur.fetchone() is not None:
                    raise ValueError("user is already in group")
                cur.execute(
                    """
                    SELECT *
                    FROM g_u_linker
                    WHERE user_id = %s AND group_id IN (
                        SELECT group_id
                        FROM meeting
                        WHERE id = %s
                    )
                    """,
                    [user_id, meeting_id],
                )
                result = cur.fetchone()
                if result is None:
                    raise ValueError(
                        "User is not a member of the meeting's group"
                    )
                cur.execute(
                    """
                INSERT INTO u_m_linker (user_id,meeting_id)
                VALUES (%s, %s)
                """,
                    [user_id, meeting_id],
                )
        return {"meeting_id": meeting_id}

    def get_u_m_linker(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT *
                    FROM u_m_linker
                    """
                )
                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)
                return results

    def delete_user_to_meeting(self, user_id, meeting_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                for user in user_id:
                    cur.execute(
                        """
                    DELETE FROM u_m_linker
                    WHERE user_id = %s AND meeting_id= %s
                    """,
                        [user, meeting_id],
                    )
        return {"meeting_id": meeting_id}

    def accept_meeting(self, user_id, meeting_id, data):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [data, user_id, meeting_id]
                cur.execute(
                    """
                    UPDATE u_m_linker
                    SET agreed_to =%s
                    WHERE user_id = %s AND meeting_id= %s
                    RETURNING agreed_to, user_id, meeting_id
                    """,
                    params,
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                return record

    def all_meetings_from_user(self, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT meeting_id
                    FROM u_m_linker
                    WHERE user_id = %s
                    """,
                    [user_id],
                )
                meeting_ids = [row[0] for row in cur.fetchall()]

            meeting_list = []
            with conn.cursor() as cur:
                for user_id in meeting_ids:
                    cur.execute(
                        """
                        SELECT id, name, descriptions, link, time, group_id
                        FROM meeting
                        WHERE id = %s
                        """,
                        [user_id],
                    )
                    c = cur.fetchone()
                    meeting_info = {
                        "id": c[0],
                        "name": c[1],
                        "descriptions": c[2],
                        "link": c[3],
                        "time": c[4],
                        "group_id": c[5],
                    }
                    meeting_list.append(meeting_info)

            return meeting_list

    def all_user_from_meeting(self, meeting_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT user_id
                    FROM u_m_linker
                    WHERE meeting_id = %s
                    """,
                    [meeting_id],
                )
                user_ids = [row[0] for row in cur.fetchall()]

            user_list = []
            with conn.cursor() as cur:
                for user_id in user_ids:
                    cur.execute(
                        """
                        SELECT id,name,email,timezone
                        FROM users
                        WHERE id = %s
                        """,
                        [user_id],
                    )
                    c = cur.fetchone()
                    meeting_info = {
                        "id": c[0],
                        "name": c[1],
                        "email": c[2],
                        "timezone": c[3],
                    }
                    user_list.append(meeting_info)

            return user_list

from fastapi.testclient import TestClient
from main import app
from api.routers.authenticator import authenticator
from api.routers.models import UserOut
from db import UserQueries, GroupQueries, MeetingQueries

client = TestClient(app)


def fake_get_current_account_data():
    return UserOut(
        name="Test",
        email="Test@Test.com",
        timezone="EST",
        picture="Test",
        id=1000,
        hashed_password="Test",
    )


class FakeUserQueries:
    def get_user(self, email):
        if email == "Test@Test.com":
            return {
                "name": "Test",
                "email": "Test@Test.com",
                "timezone": "EST",
                "picture": "Test",
                "id": 1000,
                "hashed_password": "Test",
            }
        else:
            return None


class FakeGroupQueries:
    def get_all_groups(self):
        return [
            {"id": 1, "name": "Group 1"},
            {"id": 2, "name": "Group 2"},
        ]

    def get_one_group(self, group_id):
        if group_id == 1:
            return {"id": 1, "name": "Test Group"}


class FakeMeetingQueries:
    def all_user_from_meeting(self, meeting_id):
        if meeting_id == 10:
            return ({"id": 1, "name": "User 1"},)
        else:
            return None


def test_get_user():
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    app.dependency_overrides[UserQueries] = FakeUserQueries
    email = "Test@Test.com"
    response = client.get(f"/api/users/{email}")
    assert response.status_code == 200
    app.dependency_overrides = {}
    data = response.json()
    assert "name" in data
    assert "email" in data
    assert "timezone" in data
    assert "picture" in data
    assert "id" in data
    assert "hashed_password" in data
    assert data["name"] == "Test"
    assert data["email"] == "Test@Test.com"
    assert data["timezone"] == "EST"
    assert data["picture"] == "Test"
    assert data["id"] == 1000


def test_groups_list():
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    app.dependency_overrides[GroupQueries] = FakeGroupQueries
    response = client.get("/api/groups")

    assert response.status_code == 200
    assert response.json() == {
        "groups": [
            {"id": 1, "name": "Group 1"},
            {"id": 2, "name": "Group 2"},
        ]
    }

    app.dependency_overrides = {}


def test_get_one_group():
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    app.dependency_overrides[GroupQueries] = FakeGroupQueries
    group_id = 1
    response = client.get(f"/api/groups/{group_id}")

    assert response.status_code == 200
    assert response.json() == {"id": 1, "name": "Test Group"}

    app.dependency_overrides = {}


def test_all_user_from_meeting():
    client = TestClient(app)
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    app.dependency_overrides[MeetingQueries] = FakeMeetingQueries

    meeting_id = 1
    response = client.get(f"/api/meeting/{meeting_id}/user")

    assert response.status_code == 200

    app.dependency_overrides = {}

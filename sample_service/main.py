from fastapi import FastAPI, APIRouter
from fastapi.middleware.cors import CORSMiddleware
from api.routers import users, groups, meeting
from api.routers.authenticator import authenticator
import os

app = FastAPI()
router = APIRouter

origins = [
    "http://localhost:3000",
    os.environ.get("CORS_HOST", None),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:3000")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(users.router)
app.include_router(groups.router)
app.include_router(meeting.router)
app.include_router(authenticator.router)


@app.get("/")
def read_root():
    return {"Hello": "World"}

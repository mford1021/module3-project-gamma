from fastapi import APIRouter, Depends, HTTPException
from api.routers.models import MeetingIn, MeetingOut, HttpError
from db import MeetingQueries
from api.routers.authenticator import authenticator

router = APIRouter()


@router.post("/api/meeting/", response_model=MeetingOut | HttpError)
async def create_meeting(
    user_id: int,
    meeting_in: MeetingIn,
    queries: MeetingQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    res = queries.create_meeting(meeting_in, user_id)
    return res


@router.get("/api/meeting/", response_model=MeetingOut | HttpError)
async def get_meeting(
    meeting_id: int,
    queries: MeetingQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    res = queries.get_meeting(meeting_id)
    return res


@router.put("/api/meeting/{meeting_id}")
def update_meeting(
    meeting_id: int,
    meeting_data: MeetingIn,
    queries: MeetingQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    res = queries.update_meeting(meeting_id, meeting_data)
    return res


@router.delete("/api/meeting/delete/{meeting_id}", response_model=bool)
def delete_meeting(
    meeting_id,
    queries: MeetingQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    queries.delete_meeting(meeting_id)
    return True


@router.get("/api/u-m-linker")
def u_m_linker_list(queries: MeetingQueries = Depends()):
    return {
        "u_m_linker": queries.get_u_m_linker(),
    }


@router.post("/api/meeting/add")
async def add_user_to_meeting(
    user_id: int,
    meeting_id: int,
    queries: MeetingQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        res = queries.add_user_to_meeting(user_id, meeting_id)
    except ValueError:
        raise HTTPException(status_code=400, detail="User Not In Group")
    return res


@router.delete("/api/user/meeting/delete")
async def delete_user_to_meeting(
    user_id: list[int],
    meeting_id: int,
    queries: MeetingQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    res = queries.delete_user_to_meeting(user_id, meeting_id)
    return res


@router.put("/api/accept/meeting/{meeting_id}/{user_id}")
def accept_meeting(
    meeting_id: int,
    user_id: int,
    data: bool,
    queries: MeetingQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    res = queries.accept_meeting(user_id, meeting_id, data)
    return res


@router.get("/api/user/{user_id}/meetings")
def all_meetings_from_user(
    user_id,
    queries: MeetingQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return queries.all_meetings_from_user(user_id)


@router.get("/api/meeting/{meeting_id}/user")
def all_user_from_meeting(
    meeting_id,
    queries: MeetingQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return queries.all_user_from_meeting(meeting_id)

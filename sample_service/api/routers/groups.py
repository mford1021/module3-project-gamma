from fastapi import APIRouter, Depends, HTTPException
from api.routers.models import GroupIn, GroupOut, HttpError
from db import GroupQueries
from api.routers.authenticator import authenticator
from psycopg.errors import ForeignKeyViolation


router = APIRouter()


@router.post("/api/groups/", response_model=GroupOut | HttpError)
async def create_group(
    user_id: int,
    group_in: GroupIn,
    queries: GroupQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        res = queries.create_group(user_id, group_in)
    except ValueError:
        raise HTTPException(status_code=240, detail="Invalid input")
    return res


@router.put("/api/groups/{group_id}", response_model=GroupOut | HttpError)
def update_group(
    group_id: int,
    data: GroupIn,
    queries: GroupQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        record = queries.update_group(group_id, data)
    except ValueError:
        raise HTTPException(status_code=240, detail="Invalid input")
    return record


@router.get("/api/groups")
def groups_list(
    queries: GroupQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return {
        "groups": queries.get_all_groups(),
    }


@router.get("/api/g-u-linker")
def g_u_linker_list(
    group_id,
    queries: GroupQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return queries.get_g_u_linker(group_id)


@router.get("/api/groups/{group_id}")
def get_one_group(
    group_id: int,
    queries: GroupQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    record = queries.get_one_group(group_id)
    return record


@router.get("/api/user/group/{group_id}")
def get_users_from_group(
    group_id: int,
    queries: GroupQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    record = queries.get_users_from_group(group_id)
    return record


@router.delete("/api/group/current_user", response_model=bool)
def delete_user_from_group(
    group_id,
    user_id,
    queries: GroupQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    queries.delete_user_from_group(user_id, group_id)
    return True


@router.delete("/api/group/delete/{group_id}", response_model=bool)
def delete_group(
    group_id,
    queries: GroupQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    queries.delete_group(group_id)
    return True


@router.put("/api/update/group")
def update_user_status(
    group_id: int,
    user_id: int,
    admin_status: bool,
    queries: GroupQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    res = queries.update_user_status(admin_status, user_id, group_id)
    return res


@router.get("/api/user/{user_id}/groups")
def all_groups_from_user(
    user_id: int,
    queries: GroupQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return queries.all_groups_from_user(user_id)


@router.get("/api/update/group")
def get_user_status(
    group_id: int,
    user_id: int,
    queries: GroupQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    res = queries.get_user_status(user_id, group_id)
    return res


@router.post("/api/group/add")
async def add_user_to_group(
    user_id: int,
    data: bool,
    group_id: int,
    queries: GroupQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        res = queries.add_user_to_group(user_id, group_id, data)
    except ValueError:
        raise HTTPException(status_code=400, detail="Invalid input")
    except ForeignKeyViolation:
        raise HTTPException(status_code=400, detail="Invalid input")
    return res

from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token
from typing import Optional


class User(BaseModel):
    name: str
    email: str
    timezone: str
    picture: Optional[str]


class UserIn(User):
    password: str


class UserOut(User):
    id: int
    hashed_password: str


class UsersOut(BaseModel):
    users: list[UserOut]


class UsersForm(BaseModel):
    username: str
    password: str


class UsersToken(Token):
    user: UserOut


class HttpError(BaseModel):
    detail: str


class UpdateGroupUsers(BaseModel):
    user_email: str
    is_admin: bool


class GroupIn(BaseModel):
    name: str
    news_event: Optional[str]
    users: Optional[list[UpdateGroupUsers]]


class GroupOut(GroupIn):
    id: int


class MeetingIn(BaseModel):
    name: str
    time: str
    descriptions: Optional[str]
    link: Optional[str]
    group_id: Optional[int]


class MeetingOut(MeetingIn):
    id: int

## User API's

### `GET /api/users`

- **Description**: Retrieves a list of users.
- **Request**:
  - **Headers**: None
  - **Body**: None
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: UsersOut

### `POST /api/users/`

- **Description**: Creates a new user.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `info` (UserIn)
- **Response**:
  - **Status Code**: 200 (OK) or Error
  - **Body**: UsersToken

### `PUT /api/users/{user_id}`

- **Description**: Updates a user.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `id` (int)
    - `user_in` (UserIn)
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: UserOut

### `DELETE /api/users/{user_id}`

- **Description**: Deletes a user.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `user_id` (int)
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: bool

### `GET /api/users/{email}`

- **Description**: Retrieves user information by email.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `email` (str)
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: UserOut

### `GET /api/user/current`

- **Description**: Retrieves data of the current user.
- **Request**:
  - **Headers**: None
  - **Body**: None
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: Account data

### `GET /token`

- **Description**: Retrieves the access token.
- **Request**:
  - **Headers**: None
  - **Body**: None
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: Token data

## Group API's

### `POST /api/groups/`

- **Description**: Creates a new group.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `user_id` (int)
    - `GroupIn`
- **Response**:
  - **Status Code**: 200 (OK) or Error
  - **Body**: `GroupOut`

### `PUT /api/groups/{group_id}`

- **Description**: Updates a group.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `group_id` (int)
    - `GroupIn`
- **Response**:
  - **Status Code**: 200 (OK) or Error
  - **Body**: `GroupOut`

### `GET /api/groups`

- **Description**: Retrieves a list of all groups.
- **Request**:
  - **Headers**: None
  - **Body**: None
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: `{"groups": [Group]}`

### `GET /api/g-u-linker`

- **Description**: Retrieves the link between a group and users.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `group_id` (int)
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: Linker data

### `GET /api/groups/{group_id}`

- **Description**: Retrieves information about a specific group.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `group_id` (int)
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: `Group`

### `GET /api/user/group/{group_id}`

- **Description**: Retrieves the users belonging to a specific group.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `group_id` (int)
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: Users in the group

### `DELETE /api/group/current_user`

- **Description**: Removes a user from a group.
- **Request**:
  - **Headers**: None
  - **Body**:

## Meeting API's

### `POST /api/meeting/`

- **Description**: Creates a new meeting.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `user_id` (int)
    - `meeting_in` (MeetingIn)
- **Response**:
  - **Status Code**: 200 (OK) or Error
  - **Body**: MeetingOut

### `GET /api/meeting/`

- **Description**: Retrieves a meeting.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `meeting_id` (int)
- **Response**:
  - **Status Code**: 200 (OK) or Error
  - **Body**: MeetingOut

### `PUT /api/meeting/{meeting_id}`

- **Description**: Updates a meeting.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `meeting_id` (int)
    - `meeting_data` (MeetingIn)
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: Response data

### `DELETE /api/meeting/delete/{meeting_id}`

- **Description**: Deletes a meeting.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `meeting_id` (int)
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: bool

### `GET /api/u-m-linker`

- **Description**: Retrieves the link between a user and meetings.
- **Request**:
  - **Headers**: None
  - **Body**: None
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: {"u_m_linker": Linker data}

### `POST /api/meeting/add`

- **Description**: Adds a user to a meeting.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `user_id` (int)
    - `meeting_id` (int)
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: Response data

### `DELETE /api/user/meeting/delete`

- **Description**: Deletes a user from a meeting.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `user_id` (list[int])
    - `meeting_id` (int)
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: Response data

### `PUT /api/accept/meeting/{meeting_id}/{user_id}`

- **Description**: Accepts a meeting invitation.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `meeting_id` (int)
    - `user_id` (int)
    - `data` (bool)
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: Response data

### `GET /api/user/{user_id}/meetings`

- **Description**: Retrieves all meetings associated with a user.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `user_id` (int)
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: Response data

### `GET /api/meeting/{meeting_id}/user`

- **Description**: Retrieves all users associated with a meeting.
- **Request**:
  - **Headers**: None
  - **Body**:
    - `meeting_id` (int)
- **Response**:
  - **Status Code**: 200 (OK)
  - **Body**: Response data

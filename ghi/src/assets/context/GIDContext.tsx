import {
  createContext,
  useState,
  ReactNode,
  Dispatch,
  SetStateAction,
} from "react";

interface GIDContextType {
  groupId: number | null;
  setGroupId: Dispatch<SetStateAction<number | null>>;
}

export const GIDContext = createContext<GIDContextType>({
  groupId: null,
  setGroupId: () => {
    null;
  },
});
type Props = {
  children?: ReactNode;
};

export const GIDProvider = ({ children }: Props) => {
  const [groupId, setGroupId] = useState<number | null>(null);
  const groupContextValue: GIDContextType = {
    groupId,
    setGroupId,
  };
  return (
    <GIDContext.Provider value={groupContextValue}>
      {children}
    </GIDContext.Provider>
  );
};

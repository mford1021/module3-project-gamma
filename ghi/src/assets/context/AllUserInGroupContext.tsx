import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { UserData } from "../types";
import { useContext } from "react";
import { GIDContext } from "./GIDContext";
import React, { createContext, useState } from "react";

type AllUserInGroup = {
  userInGroup: UserData[] | null;
  setUserInGroup: React.Dispatch<React.SetStateAction<UserData[] | null>>;
  fetchAllUserFromGroup: () => Promise<void>;
};

interface AllUserInGroupProviderProps {
  children: React.ReactNode;
}

export const AllUserInGroupContext = createContext<AllUserInGroup | null>(null);

export const AllUserInGroupProvider: React.FC<AllUserInGroupProviderProps> = ({
  children,
}) => {
  const { groupId } = useContext(GIDContext);
  const [userInGroup, setUserInGroup] = useState<UserData[] | null>(null);
  const { token } = useAuthContext();
  const fetchAllUserFromGroup = async () => {
    if (token !== null && groupId !== null) {
      try {
        const response = await fetch(
          `${import.meta.env.VITE_BACKEND_BASE_URL}/api/user/group/${groupId}`,
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        );
        const jsonData = await response.json();
        setUserInGroup(jsonData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    }
  };

  const contextValue: AllUserInGroup = {
    userInGroup,
    setUserInGroup,
    fetchAllUserFromGroup,
  };

  return (
    <AllUserInGroupContext.Provider value={contextValue}>
      {children}
    </AllUserInGroupContext.Provider>
  );
};

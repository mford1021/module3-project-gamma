import {
  createContext,
  useState,
  ReactNode,
  Dispatch,
  SetStateAction,
} from "react";

interface UpdateContextType {
  update: boolean;
  setUpdate: Dispatch<SetStateAction<boolean>>;
}

export const UpdateContext = createContext<UpdateContextType>({
  update: false,
  setUpdate: () => {
    false;
  },
});
type Props = {
  children?: ReactNode;
};

export const UpdateContextProvider = ({ children }: Props) => {
  const [update, setUpdate] = useState(false);
  const groupContextValue: UpdateContextType = {
    update,
    setUpdate,
  };
  return (
    <UpdateContext.Provider value={groupContextValue}>
      {children}
    </UpdateContext.Provider>
  );
};

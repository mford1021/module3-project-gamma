import {
  createContext,
  useState,
  ReactNode,
  Dispatch,
  SetStateAction,
} from "react";

interface MIDContextType {
  meetingId: number | null;
  setMeetingId: Dispatch<SetStateAction<number | null>>;
}

export const MIDContext = createContext<MIDContextType>({
  meetingId: null,
  setMeetingId: () => {
    null;
  },
});
type Props = {
  children?: ReactNode;
};

export const MIDProvider = ({ children }: Props) => {
  const [meetingId, setMeetingId] = useState<number | null>(null);
  const meetingContextValue: MIDContextType = {
    meetingId,
    setMeetingId,
  };
  return (
    <MIDContext.Provider value={meetingContextValue}>
      {children}
    </MIDContext.Provider>
  );
};

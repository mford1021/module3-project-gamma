import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { GroupData, MeetingData, UserData } from "../types";
import { useContext } from "react";
import { UserContext } from "./UserContext";

import React, { createContext, useEffect, useState } from "react";

type GMContextType = {
  groupData: GroupData[] | null;
  meetingData: MeetingData[] | null;
  fetchGroupData: () => Promise<void>;
  fetchMeetingData: () => Promise<void>;
};
interface GMContextProviderProps {
  children: React.ReactNode;
}
export const GMContext = createContext<GMContextType | null>(null);

export const GMContextProvider: React.FC<GMContextProviderProps> = ({
  children,
}) => {
  const [groupData, setGroupData] = useState<GroupData[] | null>(null);
  const [meetingData, setMeetingData] = useState<MeetingData[] | null>(null);
  const userData: UserData | null = useContext(UserContext);
  const userID: number | null = userData?.id || null;
  const { token } = useAuthContext();
  const fetchGroupData = async () => {
    if (token !== null && userID !== null) {
      try {
        const response = await fetch(
          `${import.meta.env.VITE_BACKEND_BASE_URL}/api/user/${userID}/groups`,
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        );
        const jsonData: GroupData[] = await response.json();
        setGroupData(jsonData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    }
  };

  const fetchMeetingData = async () => {
    if (token !== null && userID !== null) {
      try {
        const response = await fetch(
          `${
            import.meta.env.VITE_BACKEND_BASE_URL
          }/api/user/${userID}/meetings`,
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        );
        const jsonData: MeetingData[] = await response.json();
        setMeetingData(jsonData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    }
  };

  useEffect(() => {
    fetchMeetingData();
    // eslint-disable-next-line
  }, [token, userID]);

  useEffect(() => {
    fetchGroupData();
    // eslint-disable-next-line
  }, [token, userID]);

  const contextValue: GMContextType = {
    groupData,
    meetingData,
    fetchMeetingData,
    fetchGroupData,
  };

  return (
    <GMContext.Provider value={contextValue}>{children}</GMContext.Provider>
  );
};

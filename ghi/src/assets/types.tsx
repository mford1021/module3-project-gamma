export type UserData = {
  id: number;
  name: string;
  email: string;
  timezone: string;
  picture: string;
  password: string;
};

export type GroupData = {
  id: number;
  name: string;
  news_event: string | null;
};

export type MeetingData = {
  id: number;
  time: string;
  name: string;
  descriptions: string | null;
  link: string | null;
  group_id: number;
};

export type adminStatus = {
  user_id: number;
  group_id: number;
  admin_status: boolean;
};

export type allUserInfo = {
  id: number;
  name: string;
  email: string;
  adminStatus: boolean;
};

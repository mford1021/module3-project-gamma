import { MouseEvent } from "react";
import { useContext } from "react";
import { UserContext } from "../../assets/context/UserContext";
import { useNavigate } from "react-router-dom";
import NavHome from "./NavHome";
import { GMContext } from "../../assets/context/GMContext";
import { MIDContext } from "../../assets/context/MIDContext";
const Home = () => {
  const userData = useContext(UserContext);
  const userName = userData?.name;
  const day = new Date();
  const today = day.toDateString().substring(4, 11);
  const GMC = useContext(GMContext);
  const meetingData = GMC?.meetingData;
  const MIDC = useContext(MIDContext);
  const setMeetingId = MIDC.setMeetingId;
  const navigate = useNavigate();
  const todayMeetingFilter = meetingData?.filter(
    (meeting) => meeting.time.substring(4, 11) === today
  );
  const ViewMeetingDetail = (event: MouseEvent<HTMLButtonElement>) => {
    setMeetingId(Number(event.currentTarget.value));
    navigate("/view/meeting");
  };

  return (
    <div>
      <NavHome />
      <h1 className="text-center text-bold text-4xl">Welcome, {userName}!</h1>
      <div className="py-5">
        <p className="text-center text-bold text-3xl">
          Here is Your Meeting Today
        </p>
        {!todayMeetingFilter?.length && (
          <div className="text-center text-bold text-9xl py-5">
            You Have NO meeting TODAY WOW !!
          </div>
        )}
        <div className="grid grid-cols-1 md:grid-cols-3 gap-4 py-10 md:place-items-center">
          {todayMeetingFilter &&
            todayMeetingFilter.map((meeting) => (
              <div
                key={meeting.id}
                className="max-w-sm p-6 mb-4 rounded-lg shadow bg-gray-700 border-rose-300 border-8 min-w-[384px]"
              >
                <div>
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-slate-200 ">
                    {meeting.name}
                  </h5>
                </div>
                <p className="mb-3 font-normal  text-gray-400">
                  {meeting.descriptions}
                </p>
                <button
                  value={meeting.id}
                  onClick={ViewMeetingDetail}
                  className="inline-flex items-center px-3 py-2 text-sm font-medium text-center  bg-blue-700 rounded-lg  focus:ring-4 focus:outline-none focus:ring-blue-300   "
                >
                  Click for More Detail
                  <svg
                    aria-hidden="true"
                    className="w-4 h-4 ml-2 -mr-1"
                    fill="currentColor"
                    viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fillRule="evenodd"
                      d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    ></path>
                  </svg>
                </button>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
};

export default Home;

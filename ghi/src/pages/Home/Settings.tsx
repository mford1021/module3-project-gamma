import React from "react";
import { useState } from "react";
import { ChangeEvent } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../../assets/context/UserContext";
import { UpdateContext } from "../../assets/context/UpdateContext";
import { useContext } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import NavMeeting from "../Meetings/NavMeeting";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
export default function Setting() {
  const userData = useContext(UserContext)!;
  const timezone = ["EST", "CST", "MST", "PST", "AST", "HST"];
  const [email, setEmail] = useState(userData?.email);
  const [password, setPassword] = useState("");
  const [picture, setPicture] = useState(userData?.picture);
  const [name, setName] = useState(userData?.name);
  const [timezones, setTimezones] = useState(userData?.timezone);
  const navigate = useNavigate();
  const { token } = useAuthContext();
  const updateC = useContext(UpdateContext);
  const update = updateC.update;
  const setUpdate = updateC.setUpdate;
  const handleEmailChange = (event: ChangeEvent<HTMLInputElement>) => {
    const element = event.currentTarget as HTMLInputElement;
    setEmail(element.value);
  };
  const { login } = useToken();
  const handlePasswordChange = (event: ChangeEvent<HTMLInputElement>) => {
    const element = event.currentTarget as HTMLInputElement;
    setPassword(element.value);
  };

  const handleNameChange = (event: ChangeEvent<HTMLInputElement>) => {
    const element = event.currentTarget as HTMLInputElement;
    setName(element.value);
  };

  const handleTimeszoneChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const element = event.currentTarget;
    setTimezones(element.value);
  };

  const handlePictureChange = (event: ChangeEvent<HTMLInputElement>) => {
    const element = event.currentTarget;
    setPicture(element.value);
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const userUpdateUrl = `${
      import.meta.env.VITE_BACKEND_BASE_URL
    }/api/users/{user_id}?id=${userData.id}`;
    const data = {
      name: name,
      password: password,
      email: email,
      timezone: timezones,
      picture: picture,
    };
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await fetch(userUpdateUrl, fetchConfig);
    if (response.ok) {
      setUpdate(!update);
      login(email, password);
      navigate("/home");
    } else {
      return <div>Sorry something went wrong</div>;
    }
  };

  return (
    <div className="rounded-md">
      <NavMeeting />
      <form onSubmit={(e) => handleSubmit(e)} className="px-44 py-10">
        <div className="space-y-12">
          <div className="border-b border-gray-900/10 pb-12">
            <h2 className="text-base font-semibold leading-7 text-gray-900">
              Change Your Current Setting!
            </h2>
            <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
              <div className="sm:col-span-4">
                <label
                  htmlFor="username"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Username
                </label>
                <div className="mt-2">
                  <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                    <span className="flex select-none items-center pl-3 text-gray-500 sm:text-sm"></span>
                    <input
                      type="email"
                      name="username"
                      id="username"
                      onChange={handleEmailChange}
                      autoComplete="username"
                      value={email === null ? "" : email}
                      className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                      placeholder="janesmith@email.com"
                    />
                  </div>
                </div>
              </div>

              <div className="sm:col-span-4">
                <label
                  htmlFor="password"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Password
                </label>
                <div className="mt-2">
                  <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                    <input
                      placeholder="******"
                      onChange={handlePasswordChange}
                      type="password"
                      name="password"
                      id="password"
                      value={password}
                      className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
              </div>
              <div className="sm:col-span-4">
                <label
                  htmlFor="cover photo url"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Cover Photo Url
                </label>
                <div className="mt-2">
                  <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                    <input
                      onChange={handlePictureChange}
                      placeholder="helloWorld.picture.url.com"
                      className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="border-b border-gray-900/10 pb-12">
            <h2 className="text-base font-semibold leading-7 text-gray-900">
              Personal Information
            </h2>

            <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
              <div className="sm:col-span-3">
                <label
                  htmlFor="first-name"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Your Name
                </label>
                <div className="mt-2">
                  <input
                    onChange={handleNameChange}
                    type="text"
                    name="first-name"
                    id="first-name"
                    value={name}
                    className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                  />
                </div>
              </div>

              <div className="sm:col-span-3">
                <label
                  htmlFor="country"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Time Zone
                </label>
                <div className="mt-2">
                  <select
                    id="timezone"
                    name="timezone"
                    className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:max-w-xs sm:text-sm sm:leading-6"
                    onChange={handleTimeszoneChange}
                  >
                    <option value="">Choose a Time zone that fits you</option>
                    {timezone.map((timezone) => {
                      return (
                        <option key={timezone} value={timezone}>
                          {timezone}
                        </option>
                      );
                    })}
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="mt-6 flex items-center justify-end gap-x-6">
          <button
            type="submit"
            className="rounded-md bg-indigo-600 px-10 py-2 text-2xl font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
          >
            Change Your Setting
          </button>
        </div>
      </form>
    </div>
  );
}

import { useContext } from "react";
import { NavLink } from "react-router-dom";
import Logout from "../../components/Logout";
import { UserContext } from "../../assets/context/UserContext";
const NavHome = () => {
  const userData = useContext(UserContext);
  return (
    <div>
      <div>
        <header className="p-4 sticky top-0 bg-gray-800 text-gray-100">
          <div className="container flex flex-col justify-between h-16 mx-auto text-xl md:flex-row">
            <ul className="py-3">
              <NavLink to={"/home"}>Home</NavLink>
            </ul>
            <ul className="py-3">
              <NavLink to={"/group"}>Group</NavLink>
            </ul>
            <ul className="py-3">
              <NavLink to={"/meeting"}>Meeting</NavLink>
            </ul>

            <ul className="py-3">Your ID is {userData?.id}</ul>

            <img
              className="rounded-full object-cover"
              src={userData?.picture}
              alt="you have no picture now"
            />

            <ul className="py-3">
              <NavLink to={"/user/setting"}>Update Your Info</NavLink>
            </ul>
            <Logout />
          </div>
        </header>
      </div>
    </div>
  );
};

export default NavHome;

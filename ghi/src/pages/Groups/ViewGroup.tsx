import React, { useState, useEffect, MouseEvent } from "react";
import { UserContext } from "../../assets/context/UserContext";
import { GIDContext } from "../../assets/context/GIDContext";
import { useContext } from "react";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { GMContext } from "../../assets/context/GMContext";
import { UserData } from "../../assets/types";
import { adminStatus } from "../../assets/types";
import { allUserInfo } from "../../assets/types";
import { AllUserInGroupContext } from "../../assets/context/AllUserInGroupContext";
type props = {
  admin: boolean;
  adminStatus: adminStatus[] | null;
  setAdminStatus: React.Dispatch<React.SetStateAction<adminStatus[] | null>>;
};

const ViewGroup = ({ admin, adminStatus, setAdminStatus }: props) => {
  const GMC = useContext(GMContext);
  const groupData = GMC?.groupData;
  const { groupId } = useContext(GIDContext);
  const filteredGroup = groupData?.filter((group) => group.id === groupId);
  const { token } = useAuthContext();
  const userData = useContext(UserContext);
  const userID = userData?.id;

  const { fetchAllUserFromGroup, userInGroup } = useContext(
    AllUserInGroupContext
  )!;
  const [allUserInfo, setAllUserInfo] = useState<allUserInfo[] | null>(null);
  const [deleteUserId, setDeleteUserId] = useState<number | null>(null);

  const UserInfo = (
    userInGroup: UserData[],
    adminStatus: adminStatus[]
  ): void => {
    const userGroupData: any[] = userInGroup.map((user) => {
      const userAdminStatus = adminStatus.find(
        (status) => status.user_id === user.id
      );
      const combinedData = {
        ...user,
        adminStatus: userAdminStatus ? userAdminStatus.admin_status : false,
      };
      return combinedData;
    });

    setAllUserInfo(userGroupData);
  };

  const fetchAllUserStatusFromGroup = async () => {
    if (token !== null && userID !== null && groupId !== null) {
      try {
        const response = await fetch(
          `${
            import.meta.env.VITE_BACKEND_BASE_URL
          }/api/g-u-linker?group_id=${groupId}`,
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        );
        const jsonData = await response.json();
        setAdminStatus(jsonData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    }
  };

  const DeleteThisUser = (event: MouseEvent<HTMLButtonElement>) => {
    const userIdToDelete = Number(event.currentTarget.value);
    setDeleteUserId(userIdToDelete);
    DeleteUser(deleteUserId);
  };
  const DeleteUser = async (deleteUserId: number | null) => {
    if (token !== null && userID !== null && deleteUserId !== null) {
      const fetchConfig = {
        method: "delete",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      };
      try {
        await fetch(
          `${
            import.meta.env.VITE_BACKEND_BASE_URL
          }/api/group/current_user?group_id=${groupId}&user_id=${deleteUserId}`,
          fetchConfig
        );
      } finally {
        fetchAllUserFromGroup();
        fetchAllUserStatusFromGroup();
      }
    }
  };
  useEffect(() => {
    if (groupId !== null) {
      fetchAllUserStatusFromGroup(), fetchAllUserFromGroup();
    }
    // eslint-disable-next-line
  }, [userID, token, groupId]);

  useEffect(() => {
    if (groupId !== null && userInGroup !== null && adminStatus !== null) {
      UserInfo(userInGroup, adminStatus);
    }
    // eslint-disable-next-line
  }, [adminStatus, userInGroup]);

  return (
    <div className="py-10">
      {filteredGroup && (
        <h1 className="text-center text-4xl py-5">
          This is Group {filteredGroup[0]?.name}'s user list
        </h1>
      )}
      <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
        <table className="w-full text-sm text-left text-gray-500 ">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 ">
            <tr>
              <th scope="col" className="px-6 py-3">
                User Name
              </th>
              <th scope="col" className="px-6 py-3">
                User Email
              </th>
              <th scope="col" className="px-6 py-3">
                Admin Status
              </th>
              {admin && (
                <th scope="col" className="px-6 py-3">
                  Action
                </th>
              )}
            </tr>
          </thead>
          <tbody>
            {allUserInfo?.map((user) => (
              <tr key={user.id} className="bg-white border-b  border-gray-700">
                <th
                  scope="row"
                  className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap "
                >
                  {user.name}
                </th>
                <td className="px-6 py-4">{user.email}</td>
                <td className="px-6 py-4">{user.adminStatus.toString()}</td>
                {admin && (
                  <td className="px-6 py-4">
                    <button
                      value={user.id}
                      onClick={DeleteThisUser}
                      className="font-medium text-pink-900 hover:underline"
                    >
                      Delete User
                    </button>
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default ViewGroup;

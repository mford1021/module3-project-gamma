import React from "react";
import { useContext, useState, ChangeEvent } from "react";
import { GIDContext } from "../../assets/context/GIDContext";
import { GMContext } from "../../assets/context/GMContext";
import { GroupData } from "../../assets/types";
import { NavLink } from "react-router-dom";
import { UserContext } from "../../assets/context/UserContext";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import { useNavigate } from "react-router-dom";
const CreateMeeting = () => {
  const { groupId } = useContext(GIDContext);
  const GMC = useContext(GMContext);
  const groupData = GMC?.groupData;
  const fetchMeetingData = GMC!.fetchMeetingData;
  const filteredGroup: GroupData[] | undefined = groupData?.filter(
    (group) => group.id === groupId
  );
  const navigate = useNavigate();
  const [date, setDate] = useState(new Date());
  const [meetingName, setMeetingName] = useState<string>("");
  const [description, setDescription] = useState<string>("");
  const [link, setLink] = useState<string>("");
  const [time, setTime] = useState<string>("");
  function onChange(nextValue: any) {
    setDate(nextValue);
  }
  const userData = useContext(UserContext);
  const userID = userData?.id;
  const timeZone = userData?.timezone;
  const handleMeetingName = (event: ChangeEvent<HTMLInputElement>): void => {
    setMeetingName(event.currentTarget.value);
  };
  const handleDescription = (event: ChangeEvent<HTMLTextAreaElement>): void => {
    setDescription(event.currentTarget.value);
  };
  const handleLink = (event: ChangeEvent<HTMLInputElement>): void => {
    setLink(event.currentTarget.value);
  };
  const handleTimeSelection = (event: ChangeEvent<HTMLSelectElement>): void => {
    setTime(event.currentTarget.value);
  };
  const renderTimeOptions = () => {
    const options = [];
    for (let hour = 9; hour <= 17; hour++) {
      const time = `${hour}:00`;
      options.push(
        <option key={time} value={time}>
          {time}
        </option>
      );
    }
    return options;
  };
  const { token } = useAuthContext();
  const handleSubmit = async (
    e: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    e.preventDefault();
    const data = {
      name: meetingName,
      time: date.toDateString() + " " + time + " " + timeZone,
      descriptions: description,
      link: link,
      group_id: groupId,
    };
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await fetch(
      `${import.meta.env.VITE_BACKEND_BASE_URL}/api/meeting/?user_id=${userID}`,
      fetchConfig
    );
    if (response.ok) {
      fetchMeetingData();
      navigate("/meeting");
    }
  };
  return (
    <div>
      <header className="p-4 sticky top-0 bg-gray-800 text-gray-100">
        <nav className="pb-5">
          <ul className="flex justify-evenly">
            <li>
              <NavLink to={"/home"}>To Home</NavLink>
            </li>
            <li>
              <NavLink to={"/group"}>To Group</NavLink>
            </li>
            <li>
              <NavLink to={"/meeting"}>To Meeting</NavLink>
            </li>
          </ul>
        </nav>
      </header>
      {!groupId && (
        <div className="text-center text-bold text-2xl px-2 p-2 md:text-8xl md:px-10 md:py-10">
          Select A group to Create A meeting for{" "}
        </div>
      )}
      {filteredGroup && groupId && (
        <div>
          <div className="text-bold text-center text-4xl">
            You are creating a meeting for group {filteredGroup[0]?.name}
          </div>
          <div className="flex items-center justify-center p-12">
            <div className="mx-auto w-full max-w-[1000px]">
              <form onSubmit={handleSubmit}>
                <div className="-mx-3 flex flex-col flex-wrap">
                  <div className="w-full px-3 sm:w-1/2">
                    <div className="mb-5">
                      <label
                        htmlFor="meetingName"
                        className="mb-3 block text-base font-medium text-[#07074D]"
                      >
                        Meeting Name
                      </label>
                      <input
                        type="text"
                        name="meetingName"
                        id="meetingName"
                        onChange={handleMeetingName}
                        value={String(meetingName)}
                        placeholder="Meeting Name"
                        className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                      />
                    </div>
                  </div>

                  <div className="mb-5 px-3">
                    <label
                      htmlFor="message"
                      className="mb-3 block text-base font-medium text-[#07074D] "
                    >
                      Describe this meeting
                    </label>
                    <textarea
                      id="Meeting Description"
                      className="block p-2.5 w-full text-base font-medium text-[#07074D] bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500   "
                      placeholder="Meeting Description"
                      value={String(description)}
                      rows={8}
                      onChange={handleDescription}
                    ></textarea>
                  </div>
                </div>
                <div className="w-full px-3 sm:w-1/2">
                  <div className="mb-5">
                    <label
                      htmlFor="meetingName"
                      className="mb-3 block text-base font-medium text-[#07074D]"
                    >
                      Link/Place to This Meeting
                    </label>
                    <input
                      type="text"
                      name="Link to Meeting"
                      id="Link"
                      onChange={handleLink}
                      value={String(link)}
                      placeholder="Optional, Only if you have one"
                      className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                    />
                  </div>
                </div>
                <div className="flex flex-col justify-evenly md:flex-row">
                  <Calendar
                    onChange={onChange}
                    value={date}
                    className={"border-4"}
                    minDetail="month"
                    minDate={new Date()}
                  />
                  <div className="">
                    <div>
                      <h2>Select Meeting Time:</h2>
                      <select
                        value={String(time)}
                        onChange={handleTimeSelection}
                      >
                        <option value="">Select a time</option>
                        {renderTimeOptions()}
                      </select>
                    </div>
                  </div>
                </div>
                <div>
                  <div className="text-center bold text-4xl">
                    Selected Time For Meeting:
                    <div>
                      {date.toDateString()} {time} at Timezone {timeZone}
                    </div>
                  </div>
                  <button className="hover:shadow-form rounded-md bg-[#6A64F1] py-3 px-8 text-center text-base font-semibold text-white outline-none">
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default CreateMeeting;

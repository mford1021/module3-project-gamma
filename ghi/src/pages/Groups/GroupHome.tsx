import { useEffect, MouseEvent } from "react";
import { useContext, useState } from "react";
import CreateGroup from "./CreateGroup";
import NavGroup from "./NavGroup";
import ViewGroup from "./ViewGroup";
import { GMContext } from "../../assets/context/GMContext";
import { GIDContext } from "../../assets/context/GIDContext";
import { UserContext } from "../../assets/context/UserContext";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import UpdateGroup from "./UpdateGroup";
import GroupDropDown from "./GroupDropDown";
import AddUserToGroup from "./AddUserToGroup";
import { GroupData } from "../../assets/types";

import { Link } from "react-router-dom";
import { adminStatus } from "../../assets/types";

const GroupHome = () => {
  const [showCreate, setShowCreate] = useState(false);
  const [showUpdate, setShowupDate] = useState(false);
  const [showGroupList, setShowGroupList] = useState(false);
  const [addUserM, setAddUserM] = useState(false);
  const { token } = useAuthContext();
  const { groupId, setGroupId } = useContext(GIDContext);
  const GMC = useContext(GMContext);
  const groupData = GMC?.groupData;
  const userData = useContext(UserContext);
  const [adminStatus, setAdminStatus] = useState<adminStatus[] | null>(null);
  const userID = userData?.id;
  const [admin, setAdmin] = useState(false);
  const { fetchGroupData, fetchMeetingData } = useContext(GMContext)!;
  const DeleteGroup = async (event: MouseEvent<HTMLButtonElement>) => {
    setGroupId(null);
    if (token !== null && userID !== null && groupId !== null) {
      const fetchConfig = {
        method: "delete",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      };
      try {
        await fetch(
          `${
            import.meta.env.VITE_BACKEND_BASE_URL
          }/api/group/delete/${groupId}`,
          fetchConfig
        );
      } catch {
        console.log(event);
      } finally {
        fetchGroupData();
        fetchMeetingData();
      }
    }
  };

  const fetchAdmin = async () => {
    if (token !== null && userID !== null && groupId !== null) {
      try {
        const response = await fetch(
          `${
            import.meta.env.VITE_BACKEND_BASE_URL
          }/api/update/group?group_id=${groupId}&user_id=${userID}`,
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        );
        const jsonData = await response.json();
        setAdmin(jsonData.admin_status);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    }
  };
  useEffect(() => {
    fetchAdmin();
  }, [groupId]);

  const onClickUpdate = () => {
    setShowupDate(true);
  };

  const onClickAddUserToGroup = () => {
    setAddUserM(true);
  };

  const filteredGroup: GroupData[] | undefined = groupData?.filter(
    (group) => group.id === groupId
  );

  return (
    <section>
      <NavGroup
        showGroupList={showGroupList}
        setShowGroupList={setShowGroupList}
        showCreate={showCreate}
        setShowCreate={setShowCreate}
      />
      <GroupDropDown
        showGroupList={showGroupList}
        setShowGroupList={setShowGroupList}
      />
      {!groupId && (
        <div className="text-black text-2xl text-center md:text-6xl py-5">
          Select a group to view group details
        </div>
      )}
      {groupId && filteredGroup && (
        <div>
          <h2 className="px-10 text-6xl text-bold text-center">
            {filteredGroup[0]?.name}
          </h2>
          <div className="flex flex-col">
            {filteredGroup[0]?.news_event ? (
              <article className="text-center text-4xl pb-10">
                {filteredGroup[0]?.news_event}
              </article>
            ) : (
              <span className="text-center text-4xl pb-10">
                You have no news event
              </span>
            )}
            <div className="flex flex-row justify-evenly">
              {admin && (
                <div>
                  <button
                    onClick={onClickUpdate}
                    className="self-center px-8 py-3 mr-10 mt-4 font-semibold rounded-md bg-gray-800 text-gray-200 text-2xl"
                  >
                    Update group
                  </button>
                  <button
                    onClick={onClickAddUserToGroup}
                    className="self-center px-8 py-3 mr-10 mt-4 font-semibold rounded-md bg-gray-800 text-gray-200 text-2xl"
                  >
                    Add User to your group
                  </button>
                  <button
                    onClick={DeleteGroup}
                    className="self-center px-8 py-3 mr-10 mt-4 font-semibold rounded-md bg-gray-800 text-gray-200 text-2xl"
                  >
                    Delete This group
                  </button>
                </div>
              )}
              <Link
                to={"/create/meeting"}
                className="self-center px-8 py-3 mr-10 mt-4 font-semibold rounded-md bg-gray-800 text-gray-200 text-2xl"
              >
                Create A new Meeting
              </Link>
            </div>
          </div>
          <ViewGroup
            admin={admin}
            adminStatus={adminStatus}
            setAdminStatus={setAdminStatus}
          />
        </div>
      )}
      {addUserM && (
        <AddUserToGroup
          addUserM={addUserM}
          setAddUserM={setAddUserM}
          setAdminStatus={setAdminStatus}
        />
      )}
      {showCreate && (
        <CreateGroup showCreate={showCreate} setShowCreate={setShowCreate} />
      )}
      {showUpdate && filteredGroup && (
        <UpdateGroup
          showUpdate={showUpdate}
          filteredGroup={filteredGroup}
          setShowupDate={setShowupDate}
        />
      )}
    </section>
  );
};

export default GroupHome;

import React from "react";
import { ReactElement, useState, ChangeEvent, useContext } from "react";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { GMContext } from "../../assets/context/GMContext";
import { GIDContext } from "../../assets/context/GIDContext";
import { GroupData } from "../../assets/types";

type props = {
  showUpdate: boolean;
  setShowupDate: React.Dispatch<React.SetStateAction<boolean>>;
  filteredGroup: GroupData[];
};
const UpdateGroup = ({
  showUpdate,
  setShowupDate,
  filteredGroup,
}: props): ReactElement => {
  const { fetchGroupData } = useContext(GMContext)!;
  const { groupId } = useContext(GIDContext)!;
  const closeModal = () => {
    setShowupDate(false);
  };
  const [groupName, setGroupName] = useState<string>("");
  const [groupNews, setGroupNews] = useState<string>("");
  const handleGroupNameChange = (
    event: ChangeEvent<HTMLInputElement>
  ): void => {
    setGroupName(event.currentTarget.value);
  };
  const handleGroupNewsChange = (
    event: ChangeEvent<HTMLTextAreaElement>
  ): void => {
    setGroupNews(event.currentTarget.value);
  };
  const { token } = useAuthContext();
  const handleSubmit = async (
    e: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    e.preventDefault();
    const data = { name: groupName, news_event: groupNews };
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    try {
      await fetch(
        `${import.meta.env.VITE_BACKEND_BASE_URL}/api/groups/${groupId}`,
        fetchConfig
      );
    } catch {
      console.log(e);
    }
    fetchGroupData();
    closeModal();
  };

  return (
    <>
      {showUpdate && (
        <div>
          <div className="fixed inset-0 flex items-center justify-center z-50">
            <div className="flex flex-col max-w-lg p-6 rounded-md  md:min-w-[500px] md:min-h-[500px]  bg-gray-900 text-gray-100">
              <div className="mb-8 text-center">
                <h1 className="my-3 text-4xl font-bold">
                  Update Current Group
                </h1>
                <p className="text-sm text-gray-400">
                  Update Group {filteredGroup[0].name} for a better experience
                </p>
              </div>
              <form
                onSubmit={handleSubmit}
                className="space-y-12 ng-untouched ng-pristine ng-valid md:text-center"
              >
                <div className="space-y-4">
                  <div>
                    <label
                      htmlFor="email"
                      className="block mb-2 text-sm md:text-2xl"
                    >
                      Group Name
                    </label>
                    <input
                      className="text-gray-950"
                      onChange={handleGroupNameChange}
                      type="text"
                      value={groupName}
                    />
                  </div>
                </div>
                <div className="space-y-4">
                  <div>
                    <label
                      htmlFor="email"
                      className="block mb-2 text-sm md:text-2xl"
                    >
                      Group News Event
                    </label>
                    <textarea
                      className="text-gray-950"
                      onChange={handleGroupNewsChange}
                      value={groupNews}
                    />
                  </div>
                </div>
                <div className="space-y-2">
                  <div>
                    <button
                      type="submit"
                      className="w-full px-8 py-3 font-semibold rounded-md bg-violet-400 text-gray-900"
                    >
                      Update My Group
                    </button>
                  </div>
                </div>
              </form>
              <button
                onClick={closeModal}
                className="self-center px-8 py-3 mt-4 font-semibold rounded-md bg-violet-400 text-gray-900"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default UpdateGroup;

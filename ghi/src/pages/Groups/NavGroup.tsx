import React from "react";
import { ReactElement } from "react";
import Logout from "../../components/Logout";
import { UserContext } from "../../assets/context/UserContext";
import { useContext } from "react";
import { GMContext } from "../../assets/context/GMContext";
import { NavLink } from "react-router-dom";
type props = {
  showCreate: boolean;
  setShowCreate: React.Dispatch<React.SetStateAction<boolean>>;
  showGroupList: boolean;
  setShowGroupList: React.Dispatch<React.SetStateAction<boolean>>;
};

const NavGroup = ({
  showCreate,
  setShowCreate,
  showGroupList,
  setShowGroupList,
}: props): ReactElement => {
  const userData = useContext(UserContext);
  const userName = userData?.name;
  const { fetchGroupData } = useContext(GMContext)!;
  const handleClickCreateGroup = () => {
    setShowCreate(!showCreate);
  };
  const toggleDropdown = () => {
    setShowGroupList(!showGroupList);
    fetchGroupData();
  };
  return (
    <header className="p-4 sticky top-0 bg-gray-800 text-gray-100">
      <div className="container flex flex-col justify-between h-16 mx-auto text-xl md:flex-row">
        <span className="marker:px-8 py-3 rounded">Hello, {userName} !</span>

        <ul className="py-3">
          <NavLink to={"/home"}>Home</NavLink>
        </ul>
        <ul className="py-3">
          <NavLink to={"/meeting"}>Meeting</NavLink>
        </ul>

        <button type="button" className="px-4" onClick={toggleDropdown}>
          Check For Your Group
        </button>
        <div className="items-center flex-shrink-0 lg:flex">
          <button
            onClick={handleClickCreateGroup}
            className="self-center px-8 py-3 rounded"
          >
            Create New Group
          </button>
        </div>
        <div className="py-3">
          <Logout />
        </div>
      </div>
    </header>
  );
};

export default NavGroup;

import React from "react";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { ReactElement, useState, ChangeEvent, useContext } from "react";
import { GIDContext } from "../../assets/context/GIDContext";
import { UserContext } from "../../assets/context/UserContext";
import { AllUserInGroupContext } from "../../assets/context/AllUserInGroupContext";
type props = {
  addUserM: boolean;
  setAddUserM: React.Dispatch<React.SetStateAction<boolean>>;
  setAdminStatus: React.Dispatch<React.SetStateAction<adminStatus[] | null>>;
};
type adminStatus = {
  user_id: number;
  group_id: number;
  admin_status: boolean;
};
const AddUserToGroup = ({
  addUserM,
  setAddUserM,
  setAdminStatus,
}: props): ReactElement => {
  const { token } = useAuthContext();
  const closeModal = () => {
    setAddUserM(false);
  };
  const { groupId } = useContext(GIDContext);
  const [isAdmin, setIsAdmin] = useState(false);
  const [error, setError] = useState(false);
  const [userIdToAdd, setUserIdToAdd] = useState<number>(0);
  const handleUserIdToAdd = (event: ChangeEvent<HTMLInputElement>): void => {
    setUserIdToAdd(Number(event.currentTarget.value));
  };
  const userData = useContext(UserContext);
  const userID = userData?.id;
  const { fetchAllUserFromGroup } = useContext(AllUserInGroupContext)!;

  const fetchAllUserStatusFromGroup = async () => {
    if (token !== null && userID !== null && groupId !== null) {
      try {
        const response = await fetch(
          `${
            import.meta.env.VITE_BACKEND_BASE_URL
          }/api/g-u-linker?group_id=${groupId}`,
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        );
        const jsonData = await response.json();
        setAdminStatus(jsonData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    }
  };
  const handleSubmit = async (
    e: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    e.preventDefault();
    const data = { user_id: userIdToAdd, data: isAdmin };
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    try {
      const response = await fetch(
        `${
          import.meta.env.VITE_BACKEND_BASE_URL
        }/api/group/add?user_id=${userIdToAdd}&data=${isAdmin}&group_id=${groupId}`,
        fetchConfig
      );
      if (response.ok) {
        setUserIdToAdd(0);
        setAddUserM(false);
        fetchAllUserFromGroup();
        fetchAllUserStatusFromGroup();
      } else {
        setError(true);
      }
    } catch {
      setError(true);
    }
  };

  return (
    <div>
      {addUserM && (
        <div>
          <div className="fixed inset-0 flex items-center justify-center z-50">
            <div className="flex flex-col max-w-lg p-6 rounded-md  md:min-w-[500px] md:min-h-[500px]  bg-gray-900 text-gray-100">
              <div className="mb-8 text-center">
                <h1 className="my-3 text-4xl font-bold">
                  Add A New User to Group
                </h1>
                <div className="text-sm text-gray-400">
                  Add A new User With their ID!
                  {error && (
                    <p className="text-6xl text-white">
                      Sorry You can not add with this user ID.
                    </p>
                  )}
                </div>
              </div>
              <form
                onSubmit={handleSubmit}
                className="space-y-12 ng-untouched ng-pristine ng-valid md:text-center"
              >
                <div className="space-y-4">
                  <div>
                    <label
                      htmlFor="number"
                      className="block mb-2 text-sm md:text-2xl"
                    >
                      User ID
                    </label>
                    <input
                      onChange={handleUserIdToAdd}
                      className="text-gray-950"
                      type="number"
                      value={userIdToAdd}
                    />
                  </div>
                  <div>
                    <label
                      htmlFor="admin-status"
                      className="block mb-2 text-sm md:text-2xl"
                    >
                      Admin Status
                    </label>
                    <div>
                      <label htmlFor="admin-true" className="mr-4">
                        <input
                          id="admin-true"
                          type="radio"
                          name="admin-status"
                          value="true"
                          className="text-gray-950"
                          onChange={() => {
                            setIsAdmin(true);
                          }}
                        />
                        True
                      </label>
                      <label htmlFor="admin-false">
                        <input
                          id="admin-false"
                          type="radio"
                          name="admin-status"
                          value="false"
                          className="text-gray-950"
                          onChange={() => {
                            setIsAdmin(false);
                          }}
                        />
                        False
                      </label>
                    </div>
                  </div>
                </div>
                <div className="space-y-2">
                  <div>
                    <button
                      type="submit"
                      className="w-full px-8 py-3 font-semibold rounded-md bg-violet-400 text-gray-900"
                    >
                      Add User to This Group
                    </button>
                  </div>
                </div>
              </form>
              <button
                onClick={closeModal}
                className="self-center px-8 py-3 mt-4 font-semibold rounded-md bg-violet-400 text-gray-900"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default AddUserToGroup;

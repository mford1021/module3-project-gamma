import React from "react";
import { ReactElement, useState, ChangeEvent, useContext } from "react";
import { UserContext } from "../../assets/context/UserContext";
import { UserData } from "../../assets/types";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { GMContext } from "../../assets/context/GMContext";

type props = {
  showCreate: boolean;
  setShowCreate: React.Dispatch<React.SetStateAction<boolean>>;
};
const CreateGroup = ({ showCreate, setShowCreate }: props): ReactElement => {
  const userData: UserData | null = useContext(UserContext);
  const userID: number | null = userData?.id || null;
  const { fetchGroupData } = useContext(GMContext)!;
  const closeModal = () => {
    setShowCreate(false);
  };

  const [groupName, setGroupName] = useState<string>("");
  const handleGroupNameChange = (
    event: ChangeEvent<HTMLInputElement>
  ): void => {
    setGroupName(event.currentTarget.value);
    // this is a good way
  };
  const { token } = useAuthContext();
  const handleSubmit = async (
    e: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    e.preventDefault();
    const data = { name: groupName };
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    try {
      await fetch(
        `${
          import.meta.env.VITE_BACKEND_BASE_URL
        }/api/groups/?user_id=${userID}`,
        fetchConfig
      );
    } catch {
      console.log(e);
    }
    fetchGroupData();
    closeModal();
  };

  return (
    <>
      {showCreate && (
        <div>
          <div className="fixed inset-0 flex items-center justify-center z-50">
            <div className="flex flex-col max-w-lg p-6 rounded-md  md:min-w-[500px] md:min-h-[500px]  bg-gray-900 text-gray-100">
              <div className="mb-8 text-center">
                <h1 className="my-3 text-4xl font-bold">Create A New Group</h1>
                <p className="text-sm text-gray-400">
                  Create a new Group that help you manage your team
                </p>
              </div>
              <form
                onSubmit={handleSubmit}
                className="space-y-12 ng-untouched ng-pristine ng-valid md:text-center"
              >
                <div className="space-y-4">
                  <div>
                    <label
                      htmlFor="GroupName"
                      className="block mb-2 text-sm md:text-2xl"
                    >
                      Group Name
                    </label>
                    <input
                      className="text-gray-950"
                      onChange={handleGroupNameChange}
                      type="text"
                      value={groupName}
                    />
                  </div>
                </div>
                <div className="space-y-2">
                  <div>
                    <button
                      type="submit"
                      className="w-full px-8 py-3 font-semibold rounded-md bg-violet-400 text-gray-900"
                    >
                      Create My Group
                    </button>
                  </div>
                </div>
              </form>
              <button
                onClick={closeModal}
                className="self-center px-8 py-3 mt-4 font-semibold rounded-md bg-violet-400 text-gray-900"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default CreateGroup;

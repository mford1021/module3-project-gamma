import { NavLink } from "react-router-dom";
import Logout from "../../components/Logout";
import { UserContext } from "../../assets/context/UserContext";
import { useContext } from "react";

const NavMeeting = () => {
  const userData = useContext(UserContext);
  const userName = userData?.name;
  return (
    <div>
      <header className="p-4 sticky top-0 bg-gray-800 text-gray-100">
        <div className="container flex flex-col justify-between h-16 mx-auto text-xl md:flex-row">
          <span className="marker:px-8 py-3 rounded">Hello, {userName} !</span>

          <ul className="py-3">
            <NavLink to={"/home"}>Home</NavLink>
          </ul>
          <ul className="py-3">
            <NavLink to={"/group"}>Group</NavLink>
          </ul>
          <ul className="py-3">
            <NavLink to={"/meeting"}>Meeting</NavLink>
          </ul>
          <div className="py-3">
            <Logout />
          </div>
        </div>
      </header>
    </div>
  );
};

export default NavMeeting;

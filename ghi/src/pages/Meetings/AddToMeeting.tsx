import React from "react";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { ReactElement, useState, ChangeEvent, useContext } from "react";
import { MIDContext } from "../../assets/context/MIDContext";

type props = {
  addUserToMeetingM: boolean;
  setAddUserToMeetingM: React.Dispatch<React.SetStateAction<boolean>>;
  fetchAllUserInMeeting: () => Promise<void>;
};

const AddUserToMeeting = ({
  addUserToMeetingM,
  setAddUserToMeetingM,
  fetchAllUserInMeeting,
}: props): ReactElement => {
  const { token } = useAuthContext();
  const closeModal = () => {
    setAddUserToMeetingM(false);
    setError(false);
  };
  const [error, setError] = useState(false);
  const [userIdToAdd, setUserIdToAdd] = useState<number>(0);
  const handleUserIdToAdd = (event: ChangeEvent<HTMLInputElement>): void => {
    setUserIdToAdd(Number(event.currentTarget.value));
  };
  const { meetingId } = useContext(MIDContext);
  const handleSubmit = async (
    e: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    e.preventDefault();
    const fetchConfig = {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await fetch(
      `${
        import.meta.env.VITE_BACKEND_BASE_URL
      }/api/meeting/add?user_id=${userIdToAdd}&meeting_id=${meetingId}`,
      fetchConfig
    );
    if (response.ok) {
      setUserIdToAdd(0);
      setAddUserToMeetingM(false);
      fetchAllUserInMeeting();
    } else {
      setError(true);
      setUserIdToAdd(0);
    }
  };

  return (
    <>
      {addUserToMeetingM && (
        <div>
          <div>
            <div className="fixed inset-0 flex items-center justify-center z-50">
              <div className="flex flex-col max-w-lg p-6 rounded-md  md:min-w-[500px] md:min-h-[500px]  bg-gray-900 text-gray-100">
                <div className="mb-8 text-center">
                  <h1 className="my-3 text-4xl font-bold">
                    Add A New User to Meeting
                  </h1>
                  <div className="text-sm text-gray-400">
                    Add A new User With their ID!
                    {error && (
                      <p className="text-3xl text-white">
                        Sorry, you can not add user to meeting with this ID
                      </p>
                    )}
                  </div>
                </div>
                <form
                  onSubmit={handleSubmit}
                  className="space-y-12 ng-untouched ng-pristine ng-valid md:text-center"
                >
                  <div className="space-y-4">
                    <div>
                      <label
                        htmlFor="number"
                        className="block mb-2 text-sm md:text-2xl"
                      >
                        User ID
                      </label>
                      <input
                        onChange={handleUserIdToAdd}
                        className="text-gray-950"
                        type="number"
                        value={userIdToAdd}
                      />
                    </div>
                  </div>
                  <div className="space-y-2">
                    <div>
                      <button
                        type="submit"
                        className="w-full px-8 py-3 font-semibold rounded-md bg-violet-400 text-gray-900"
                      >
                        Add User to Meeting
                      </button>
                    </div>
                  </div>
                </form>
                <button
                  onClick={closeModal}
                  className="self-center px-8 py-3 mt-4 font-semibold rounded-md bg-violet-400 text-gray-900"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default AddUserToMeeting;

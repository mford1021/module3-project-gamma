import { Link } from "react-router-dom";
const StartFree = () => {
  return (
    <>
      <Link to="/signup">
        <button className="font-bold  bg-sky-300 text-white animate-[bounce_2s_infinite] hover:animate-none">
          <p>START FREE</p> <p> YES FREE</p>
        </button>
      </Link>
    </>
  );
};
export default StartFree;

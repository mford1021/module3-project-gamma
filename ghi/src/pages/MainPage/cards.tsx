import letuswork from "../../assets/MainPageImage/letusdothework.png";
import vec from "../../assets/MainPageImage/vector-work-2-removebg-preview.png";
const Cards = () => {
  return (
    <div>
      <section className="flex px-6 flex-col pt-10 md:pt-25 md:flex-row md:px-10">
        <div className="px-4">
          <h1 className="text-4xl font-bold pb-5">
            Let us do the work <span className="text-red-700">.</span>
          </h1>
          <p className="text-lg text-slate-500, md:text-2xl">
            Spend more time providing memorable group experiences. Your free
            booking system handles the legwork so you don’t have to.
          </p>
        </div>
        <div>
          <img
            className="md:min-w-[500px]"
            src={letuswork}
            alt="let us do the work image "
          />
        </div>
      </section>
      <section className="flex pt-10 flex-col-reverse px-6 md:pt-25 md:flex-row">
        <div>
          <img
            className="md:min-w-[500px]"
            src={vec}
            alt="let us do the work image "
          />
        </div>
        <div className="px-4 md:pt-12">
          <h1 className="text-4xl font-bold pb-5">
            Give clients the power to book any time
            <span className="text-sky-700">.</span>
          </h1>
          <p className="text-lg text-slate-500, md:text-2xl">
            With all-hours self-booking, free online scheduling application that
            helps manage your calender.
          </p>
        </div>
      </section>
    </div>
  );
};

export default Cards;

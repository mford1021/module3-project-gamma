import StartFree from "../Buttons/StartFree";
import { useState, ReactElement } from "react";
import { StepInfo } from "./StepInfo";
import judy from "../../assets/MainPageImage/judy-removebg-preview.png";
import google from "../../assets/MainPageImage/google.png";
import cal from "../../assets/MainPageImage/svg-cal.svg";
import connect from "../../assets/MainPageImage/connect.svg";
import timezone from "../../assets/MainPageImage/timezone.svg";
import email from "../../assets/MainPageImage/email.svg";
import Cards from "./cards";
import FAqustion from "./FAQ";
import NavMain from "./NavMain";
import Login from "../../components/Login";

const MainPage = (): ReactElement => {
  const [showLogin, setShowLogin] = useState(false);

  return (
    <>
      <NavMain showLogin={showLogin} setShowLogin={setShowLogin} />
      {showLogin && <Login showLogin={showLogin} setShowLogin={setShowLogin} />}
      <main className="max-w-[1500px] m-auto">
        <section className="flex flex-col-reverse py-18 px-18 md:flex-row">
          <article className="flex flex-col md:px-8">
            <h1 className="font-black text-2xl py-6 px-8 md:text-5xl">
              <div className="hover:text-sky-400">
                The Best <span className="text-slate-400">FREE</span> scheduling
                software in the market
              </div>
            </h1>
            <p className="font-bold text-base py-2 md:text-2xl">
              Organize Your business with 24/7 automated online booking,
              reminders, group connection, broadcasting and more.
            </p>
            <div className="text-3xl py-3 self-center">
              <StartFree />
            </div>
            <div className="font-medium text-center">
              "There is no app that can be better than this"
              &#10024;&#10024;&#10024;&#10024;&#10024;
            </div>
            <div className="self-center font-medium">
              From Google
              <img style={{ maxWidth: "100px" }} src={google} alt="google" />
            </div>
          </article>
          <figure className="self-center">
            <img
              className="max-w-[368px] md:min-w-[530px]"
              src={judy}
              alt="watching"
            />
          </figure>
        </section>
        <section className="py-24 px-10 flex flex-warp flex-col justify-around md:px-24 md:flex-nowrap md:flex-row">
          <div className="max-w-prose px-3 pb-10">
            <div>
              <img className="w-20 pb-3" src={cal} alt="cal" />
            </div>
            <h2 className="font-semibold text-3xl pb-6">Stay one step ahead</h2>
            <p className="text-xl">
              Share your online Booking Page and every new appointment lands in
              your calendar.
            </p>
          </div>
          <div className="max-w-prose px-3 pb-10">
            <div>
              <img className="w-20 pb-3" src={connect} alt="cal" />
            </div>
            <h2 className="font-semibold text-3xl pb-6">
              Feel and be Connected
            </h2>
            <p className="text-xl">
              Have the ability to connect with anyone you know, and keep them
              together
            </p>
          </div>
          <div className="max-w-prose px-3 pb-10">
            <div>
              <img className="w-20 pb-3" src={timezone} alt="cal" />
            </div>
            <h2 className="font-semibold text-3xl pb-6">
              Forget Time difference
            </h2>
            <p className="text-xl">
              Share your online Booking Page and every new appointment lands in
              your calendar.
            </p>
          </div>
          <div className="max-w-prose px-3 pb-10">
            <div>
              <img className="w-20 pb-3" src={email} alt="cal" />
            </div>
            <h2 className="font-semibold text-3xl pb-6">No more no-shows</h2>
            <p id="step" className="text-xl">
              Let SyncUp fire out personalized email reminders to every customer
              or friends.
            </p>
          </div>
        </section>
        <StepInfo />
        <Cards />
        <FAqustion />
        {/* <AboutUs /> */}
      </main>
    </>
  );
};

export default MainPage;

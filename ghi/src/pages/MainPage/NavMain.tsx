import { ReactElement } from "react";
import React from "react";
import syncup from "../../assets/logos/SyncUp-1.png";
import { Link } from "react-router-dom";
type props = {
  showLogin: boolean;
  setShowLogin: React.Dispatch<React.SetStateAction<boolean>>;
};

const NavMain = ({ showLogin, setShowLogin }: props): ReactElement => {
  const handleClickSignIn = () => {
    setShowLogin(!showLogin);
  };

  return (
    <header className="p-4 sticky top-0 bg-gray-800 text-gray-100">
      <div className="container flex justify-between h-16 mx-auto">
        <img src={syncup} alt="logo" />
        <ul className="items-stretch hidden space-x-3 md:flex">
          <li className="flex">
            <a
              rel="noopener noreferrer"
              href="#"
              className="flex items-center px-4 -mb-1 border-b-2 border-transparent active:text-violet-400 active:border-violet-400"
            >
              Main Page
            </a>
          </li>
          <li className="flex">
            <a
              rel="noopener noreferrer"
              href="#step"
              className="flex items-center px-4 -mb-1 border-b-2 border-transparent active:text-violet-400 active:border-violet-400"
            >
              Your way to Success
            </a>
          </li>

          <li className="flex">
            <a
              rel="#FAQ"
              href="#FAQ"
              className="flex items-center px-4 -mb-1 border-b-2 border-transparent active:text-violet-400 active:border-violet-400"
            >
              FAQ
            </a>
          </li>
        </ul>
        <div className="items-center flex-shrink-0  lg:flex">
          <button
            onClick={handleClickSignIn}
            className="self-center px-8 py-3 rounded"
          >
            Sign in
          </button>
          <Link
            className="self-center px-8 py-3 font-semibold rounded bg-violet-400 dark:text-gray-900"
            to="/signup"
          >
            Sign up
          </Link>
        </div>
      </div>
    </header>
  );
};

export default NavMain;

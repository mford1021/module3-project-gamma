import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./pages/Home/Home";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import { UserContextProvider } from "./assets/context/UserContext";
import { GMContextProvider } from "./assets/context/GMContext";
import { lazy, Suspense } from "react";
import Loading from "./components/Loading";
import Footer from "./components/Footer";
import { GIDProvider } from "./assets/context/GIDContext";
import CreateMeeting from "./pages/Groups/CreateMeeting";
import { AllUserInGroupProvider } from "./assets/context/AllUserInGroupContext";
import { MIDProvider } from "./assets/context/MIDContext";
import ViewMeeting from "./pages/Meetings/ViewMeeting";
import UpdateMeeting from "./pages/Meetings/UpdateMeeting";
import { UpdateContextProvider } from "./assets/context/UpdateContext";
function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = import.meta.env.VITE_PUBLIC_URL.replace(domain, "");
  const baseU = import.meta.env.VITE_BACKEND_BASE_URL;
  const MainPage = lazy(() => import("./pages/MainPage/MainPage"));
  const GroupHome = lazy(() => import("./pages/Groups/GroupHome"));
  const SignUp = lazy(() => import("./components/SignUp"));
  const MeetingHome = lazy(() => import("./pages/Meetings/MeetingHome"));
  const Setting = lazy(() => import("./pages/Home/Settings"));
  const Notfound = lazy(() => import("./pages/error/notfound"));
  return (
    <AuthProvider baseUrl={baseU}>
      <UpdateContextProvider>
        <UserContextProvider>
          <GMContextProvider>
            <GIDProvider>
              <AllUserInGroupProvider>
                <MIDProvider>
                  <div className="bg-sky-100 min-h-screen">
                    <BrowserRouter basename={basename}>
                      <Suspense fallback={<Loading />}>
                        <div className="min-h-screen">
                          <Routes>
                            <Route path="/home" element={<Home />} />
                            <Route path="/meeting" element={<MeetingHome />} />
                            <Route
                              path="/create/meeting"
                              element={<CreateMeeting />}
                            />
                            <Route
                              path="/view/meeting"
                              element={<ViewMeeting />}
                            />
                            <Route path="/group" element={<GroupHome />} />
                            <Route path="/" element={<MainPage />} />
                            <Route path="/signup" element={<SignUp />} />
                            <Route path="*" element={<Notfound />} />
                            <Route
                              path="/meeting/update"
                              element={<UpdateMeeting />}
                            />
                            <Route path="/user/setting" element={<Setting />} />
                          </Routes>
                        </div>
                        <Footer />
                      </Suspense>
                    </BrowserRouter>
                  </div>
                </MIDProvider>
              </AllUserInGroupProvider>
            </GIDProvider>
          </GMContextProvider>
        </UserContextProvider>
      </UpdateContextProvider>
    </AuthProvider>
  );
}
export default App;
